import mongoose from '@/database';
import bcrypt from 'bcryptjs';

const UserSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
  },
  password: {
    type: String,
    required: true,
    select: false,
  },
  name: {
    type: String,
    required: true,
  },
  rank: {
    type: String,
    required: true,
  },
  permission: {
    type: Number,
    default: '0',
  },
  registerDate: {
    type: Date,
    default: Date.now,
  },
  passwordResetToken: {
    type: String,
    select: false,   
  },
  passwordResetTokenExpiration: {
    type: Date,
    select: false,   
  },
});

UserSchema.pre('save', function(next){
  bcrypt.hash(this.password, 10).then(hash => {
    this.password = hash;
    next();
  }).catch(error => {
    console.error('Error hashing password', error);
  })
  
});

export default mongoose.model('User', UserSchema);
