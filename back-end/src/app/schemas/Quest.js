import mongoose from '@/database';
import Slugify from '@/utils/Slugify';

const QuestSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    unique: true,
  },
  slug: {
    type: String,
    unique: true,
  },
  rank: {
    type: String,
    required: true,
  },
  typeQuest: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  reward: {
    type: String,
    required: true,
  },
  datePost: {
    type: Date,
    default: Date.now,
  },
});

QuestSchema.pre('save', function(next){
    const title = this.title;
    this.slug = Slugify(title);
    next();
});

export default mongoose.model('Quest', QuestSchema);
