import { Router } from 'express';
import QuestSchema from '@/app/schemas/Quest';
import { isValidObjectId } from 'mongoose'; //ignorem essa importação de início
import UserMiddleware from '@/app/middlewares/Auth';
import AdminMiddleware from '@/app/middlewares/Admin';
import Slugify from '@/utils/Slugify';
const router = new Router();

router.get('/hello', (req, res) => { //rota simples de teste (NÃO DOCUMENTADA - COMENTAR)
  return res.send({ message: 'Quests legais.' });
});

router.post('/register', AdminMiddleware, (req, res) => { //Registra uma Quest no Painel (somente admin logado)
  const { title, slug, rank, typeQuest, description, reward } = req.body;

  if (!title) return res.status(400).send({ erro: 'É necessário definir um título para a quest!' });
  if (!rank) return res.status(400).send({ erro: 'É necessária definir um rank para a quest!' });
  if (!typeQuest) return res.status(400).send({ erro: 'É necessária definir um tipo para a quest!' });
  if (!description) return res.status(400).send({ erro: 'É necessário definir um descrição para a quest!' });
  if (!reward) return res.status(400).send({ erro: 'É necessária definir uma recompensa para a quest!' });
  
  QuestSchema.create({ title, slug, rank, typeQuest, description, reward})
    .then((resultado) => {
      return res.send(resultado);
    })
    .catch(error => {
      console.error('Erro ao salvar quest.', error);
      return res.status(500).send({error: 'Registration failed.'});
  });
});

router.get('/', (req, res) => { //Lista as Quests do Painel de forma reduzida
  QuestSchema.find()
    .then((resultado) => {
        const formatacao = resultado.map(format => {return {title: format.title, rank: format.rank, typeQuest: format.typeQuest, slug: format.slug}});
        return res.send(formatacao);
    })
    .catch(error => {
      console.error('Erro ao listar objetos.', error);
      return res.status(500).send({error: 'Internal server error.'});
  });
});

router.get('/:questSlug', UserMiddleware, (req, res) => { //Lista detalhadamente uma Quest específica (pelo slug) (somente usuário logado)
    QuestSchema.findOne({slug: req.params.questSlug})
      .then(resultado => {
        return res.send(resultado);
      })
      .catch(error => {
        console.error('Erro ao listar objeto.', error);
        return res.status(500).send({error: 'Internal server error.'});
    });
  });

router.put('/:id', AdminMiddleware, (req, res) => { //Edita uma Quest específica (pelo Id) (somente admin logado)
  //:id significa que id é um parâmetro da requisição
  //é possivel obter o id através de req.params.id
  const id = req.params.id;
  const { title, rank, typeQuest, description, reward } = req.body;
  let slug = undefined;
  if (!title) {
    return res.status(400).send({ erro: 'É necessário definir um título para a quest!' });
  } else {
    slug = Slugify(title);
  }
  
  if (!rank) return res.status(400).send({ erro: 'É necessária definir um rank para a quest!' });
  if (!typeQuest) return res.status(400).send({ erro: 'É necessária definir um tipo para a quest!' });
  if (!description) return res.status(400).send({ erro: 'É necessário definir um descrição para a quest!' });
  if (!reward) return res.status(400).send({ erro: 'É necessária definir uma recompensa para a quest!' });
  
  if (!id) return res.status(400).send({ erro: 'ID é obrigatório' });
  if (!isValidObjectId(id))
    return res.status(400).send({ erro: 'ID inválido' });

    QuestSchema.findByIdAndUpdate(id, { title, slug, rank, typeQuest, description, reward })
    .then((resultado) => {
      if (resultado) return res.send(resultado);
      //Retorna o resultado antes da mudança, apesar de ter modificado
      else return res.status(404).send({ erro: 'Object not found.' });

      //Quando algum método de find do mongoose vai para o then, significa que o processo de busca deu certo,
      //mas não significa que foi encontrado, por isso é necessário o if e else
    })
    .catch((err) => {
      console.error(err, 'Erro ao editar o objeto');
      return res.status(500).send({ erro: 'Internal server error.' });
    });
});

router.delete('/:id', AdminMiddleware, (req, res) => { //Deleta uma Quest específica (pelo Id) (somente admin logado)
  const id = req.params.id;

  if (!id) return res.status(400).send({ erro: 'ID é obrigatório' });
  if (!isValidObjectId(id))
    return res.status(400).send({ erro: 'ID inválido' });

    QuestSchema.findByIdAndRemove(id)
    .then((resultado) => {
      if (resultado) return res.send(resultado);
      else return res.status(404).send({ erro: 'Object not found.' });
    })
    .catch((err) => {
      console.error(err, 'Erro ao remover objeto');
      return res.status(500).send({ erro: 'Internal server error.' });
    });
});

export default router;
