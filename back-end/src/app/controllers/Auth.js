import { Router } from 'express';
import bcrypt from 'bcryptjs';
import authConfig from '@/config/auth';
import crypto from 'crypto';
import jwt from 'jsonwebtoken';
import Mailer from '@/modules/Mailer';
import User from '@/app/schemas/User';
import AdminMiddleware from '@/app/middlewares/Admin';
import { isValidObjectId } from 'mongoose'; //ignorem essa importação de início

const router = new Router();

const generateToken = params => {
    return jwt.sign(params, authConfig.secret,{
        expiresIn: 86400
    });
}

router.post('/register', (req, res) => { //Rota de registro de usuários
    const {email, name, password, rank} = req.body;
    
    User.findOne({ email }).then(userData =>{ //Não permite emails repetidos
        if(userData) {
            return res.status(400).send({error: 'User already exists.'})
        } else {
            User.create({name, email, password, rank}).then(user => {
                //user.password = undefined;
                return res.send({user});
            }).catch(error => {
                console.error('Erro ao salvar usuario.', error);
                return res.status(500).send({error: 'Registration failed.'});
            });
        }
    }).catch(error => {
        console.error('Erro ao consultar usuario no banco de dado.', erro);
        return res.status(500).send({error: 'Registration failed.'});
    })
})

router.post('/login', (req, res) => { //Rota de login de usuários
    const {email, password} = req.body;
    User.findOne({ email })
    .select('+password')
    .then(userData =>{
        if(userData) {
            bcrypt.compare(password, userData.password)
            .then(result => {
                if(result) {
                    /*userData.password = undefined;
                    return res.send({userData});*/

                    const token = generateToken({uid: userData.id});
                    return res.send({token: token, tokenExperation: '1d'});
                } else {
                    return res.status(400).send({error: 'Invalid password.'});
                }
            })
        } else {
            return res.status(404).send({error: 'User not found.'});
        }
    }).catch(error => {
        console.error('Erro ao logar.', erro);
        return res.status(500).send({error: 'Internal server error.'});
    })
})

router.post('/forget-password', (req, res) => { //Rota que faz o um envio de email de recuperação para o usuário
    const {email} = req.body;
    User.findOne({ email })
    .then(user =>{
        if(user){
            const token = crypto.randomBytes(20).toString('hex');
            const expiration = new Date();
            expiration.setHours(new Date().getHours() + 3);

            User.findByIdAndUpdate(user.id, {
                $set: {
                    passwordResetToken: token,
                    passwordResetTokenExpiration: expiration
                }
            }).then(() => {
                Mailer.sendMail({
                    to: email,
                    from: 'webmaster@testexpress.com',
                    template: 'auth/forgot_password',
                    context: {token}
                }, error => {
                    if(error) {
                        console.error('Erro ao enviar email.', error);
                        return res.status(400).send({error: 'Fail sending recover password mail.'})
                    } else {
                        return res.send({message: 'Email sent.'});
                    }

                })
            }).catch(error => {
                console.error('Erro ao salvar o token de recuperação de senha.', erro);
                return res.status(500).send({error: 'Internal server error.'});
            });

        } else {
            return res.status(404).send({error: 'User not found.'});
        }

    }).catch(error => {
        console.error('Erro no forgot password.', erro);
        return res.status(500).send({error: 'Internal server error.'});
    });

})

router.post('/reset-password', (req, res) => { //Rota para alterar a senha usuário após receber o email de recuperção
    const {email, token, newPassword} = req.body;

    User.findOne({email})
    .select('+passwordResetToken passwordResetTokenExpiration')
    .then(user => {
        if(user) {
            if(token != user.passwordResetToken || new Date().now > user.passwordResetTokenExpiration){
                return res.status(400).send({error: 'Invalid token.'});
            } else {
                user.passwordResetToken = undefined;
                user.passwordResetTokenExpiration = undefined;
                user.password = newPassword;

                user.save().then(() => {
                    return res.send({message: 'Senha trocada com sucesso.'});
                }).catch(error => {
                    console.error('Erro ao salvar nova senha do usuário.', erro);
                    return res.status(500).send({error: 'Internal server error.'});
                })
            }


        }else {
            return res.status(404).send({error: 'User not found.'});
        }
    }).catch(err => {
        console.error('Erro no forgot password.', err);
        return res.status(500).send({error: 'Internal server error.'});
    });
})

router.delete('/:id', AdminMiddleware, (req, res) => { //Deleta usuário (NÃO DOCUMENTADA - COMENTAR)
    const id = req.params.id;
  
    if (!id) return res.status(400).send({ erro: 'ID é obrigatório' });
    if (!isValidObjectId(id))
      return res.status(400).send({ erro: 'ID inválido' });
  
      User.findByIdAndRemove(id)
      .then((resultado) => {
        if (resultado) return res.send(resultado);
        else return res.status(404).send({ erro: 'User not found.' });
      })
      .catch((err) => {
        console.error(err, 'Erro ao remover usuário');
        return res.status(500).send({ erro: 'Internal server error.' });
      });
  });

export default router;



