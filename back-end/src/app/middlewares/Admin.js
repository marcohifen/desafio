import jwt from 'jsonwebtoken';
import authConfig from '@/config/auth';
import User from '@/app/schemas/User';

export default (req, res, next) => { //Verifica se está logado (Possui token válido) e verifica se é admin (permission = 1)
    const authHeader = req.headers.authorization;
    if(authHeader) {
        const tokenData = authHeader.split(' ');
        if(tokenData.length !== 2) {
            return res.status(401).send({error: 'No valid token provided.'}); 
        }

        const [scheme, token] = tokenData;
        if(scheme.indexOf('Bearer') < 0) {
            return res.status(401).send({error: 'No valid token provided.'});
        }

        jwt.verify(token, authConfig.secret, (err, decoded) => {
            if(err) {
                return res.status(401).send({error: 'No valid token provided.'});
            } else {
                req.uid = decoded.uid;
                User.findById(decoded.uid, function (err, userdata) {
                    if (err || !userdata){
                        return res.status(404).send({error: 'Invalid user id or user not found.'});
                    } else {
                        if(userdata.permission == 1){
                            return next();
                        } else {
                            return res.status(403).send({error: 'Not allowed.'});
                        }
                    }
                });
            }
        })
    } else {
        return res.status(401).send({error: 'No valid token provided.'});
    }
}