//import bodyParser from 'body-parser';
//import cors from 'cors';
//import { frontUrl } from './config/urls';

import { QuestPanel, Auth } from '@/app/controllers';
import express from 'express';

const app = express();

const port = 3000;

//app.use(bodyParser.json()); //deprecated
//app.use(bodyParser.urlencoded({ extended: false })); //deprecated
app.use(express.json());
app.use(express.urlencoded({ extended: false })); 

app.use('/questpanel', QuestPanel);
app.use('/auth', Auth);

app.listen(port, () => {
  console.log(`Servidor rodando no link http://localhost:${port}`);
});
