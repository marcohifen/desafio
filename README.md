# Codigo desafio de treinee - BackEnd

Desafio de capaciatação:

CRUD de um painel de missões.
Login de usuário com recuperação de senha por email.
Autenticação por Bearer token.

## Instalação

### BackEnd

```shell
cd ../back-end
npm install
npm run serve
```

## Build

```shell

cd ../back-end
npm install
npm run dev
```

## Membros

PS 2021/1
Trainee: Marco
Mentor: BO

## Tecnologias utilizadas

Back End: Nodejs
